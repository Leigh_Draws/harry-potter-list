
// Affichage liste perso dans le DOM

let characterListContainer = document.getElementById("characters-list")

// Fonction pour afficher la liste des personnages

function showCharacterList() {

    for (const eachCharacter of characterList) {

        let characterCard = document.createElement("article");
        characterCard.classList.add("card");

        const characterName = document.createElement("h2");
        characterName.textContent = eachCharacter.name;

        const characterPicture = document.createElement("img");
        characterPicture.src = eachCharacter.image;
        
        const characterHouse = document.createElement("h4");
        characterHouse.textContent = "House : " + eachCharacter.house;

        const characterGender = document.createElement("p");
        characterGender.textContent = "Gender : " + eachCharacter.gender;

        const characterDateofBirth = document.createElement("p");
        characterDateofBirth.textContent = "Date of Birth : " + eachCharacter.dateOfBirth;

        const characterEyes = document.createElement("p");
        characterEyes.textContent = "Eyes : " + eachCharacter.eyeColour;

        const characterHair = document.createElement("p");
        characterHair.textContent = "Hair : " + eachCharacter.hairColour;

        const characterPatronus = document.createElement("p");
        characterPatronus.textContent = "Patronus : " + eachCharacter.patronus;

        

        characterCard.appendChild(characterName)
        characterCard.appendChild(characterPicture)
        characterCard.appendChild(characterHouse)
        characterCard.appendChild(characterGender)
        characterCard.appendChild(characterDateofBirth)
        characterCard.appendChild(characterEyes)
        characterCard.appendChild(characterHair) 
        characterCard.appendChild(characterPatronus)

        characterListContainer.appendChild(characterCard)

    }
}

showCharacterList()


// Fonction fléchée qui créée une map (un nouveau tableau) avec seulement les acteurs

const actorsList = characterList.map(actor => {
    return actor.actor
})

console.log( "liste d'acteurs : " + actorsList)

// Filtrer la liste en fct des maisons
// 4 maisons :
// - Gryffindor
// - Slytherin
// - Ravenclaw
// - Hufflepuff

const filterCharacterByHouse = (list, house) => {

    const houseFiltered = list.filter((eachCharacter) => {
        // Maison : Gryffindor 
        if (house === "Gryffindor") {
            if (eachCharacter.house === "Gryffindor") {
                return true;
            } else {
                return false;
            }

        // Maison : Ravenclaw
        } else if (house === "Ravenclaw") {
            if (eachCharacter.house === "Ravenclaw") {
                return true;
            } else {
                return false;
            }

        // Maison : Hufflepuff
        } else if (house === "Hufflepuff") {
            if (eachCharacter.house === "Hufflepuff") {
                return true;
            } else {
                return false;
            }

        // Maison : Slytherin 
        } else if (house === "Slytherin") {
            if (eachCharacter.house === "Slytherin") {
                return true;
            } else {
                return false;
            }
        }
    })
    return houseFiltered
}


console.log(filterCharacterByHouse(characterList, "Slytherin"))

// Fonction qui renvoie les sorciers en vie

function nameWizardsAlive(listePersonnages) {
    return listePersonnages
      .filter(function (personnage) {
        return personnage.alive === true;
      })
      .map(function (personnage) {
        return personnage.name;
      });
  }
  
  // Fonction à utiliser
  let nomsSorciersVivants = nameWizardsAlive(characterList);
  console.log("Sorcier vivants: " + nomsSorciersVivants);

// Autre fonction qui renvoie les sorciers vivants

const getWizardsAlive = (wizardsList) => {
    const wizardsAlive = wizardsList.filter((eachwizard) => {
        if (eachwizard.alive === true) {
            return true;
        } else {
            return false;
        }
    });
    return wizardsAlive;
  }
  
  console.log(getWizardsAlive(characterList))
  

//  Fonction qui renvoie une liste de personnages sur une recherche par nom incomplet (Weasley)

